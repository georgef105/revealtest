// has 3 equal sides
const EQUILATERAL = "EQUILATERAL";
// has 2 equal sides
const ISOCELES = "ISOCELES";
// has no equal sides
const SCALENE = "SCALENE";

const EQUAL_SIDE_COUNT_TO_TYPE_MAP = {
  0: SCALENE,
  2: ISOCELES,
  3: EQUILATERAL,
};

const validateTriangleSides = (a, b, c) => {
  const sides = [a, b, c];

  const sidesAreNumbers = sides.every((side) => !Number.isNaN(side));
  if (!sidesAreNumbers) {
    return {
      isValid: false,
      message: "Invalid sides format. All sides must be a valid number",
    };
  }

  return {
    isValid: true,
  };
};

const getTriangleType = (a, b, c) => {
  const sides = [a, b, c];

  const equalSideTotal = sides.reduce((equalSidesCount, sideLength, index) => {
    const hasAnEqualSide = sides.some(
      (side, childIndex) => index !== childIndex && side === sideLength
    );
    return hasAnEqualSide ? equalSidesCount + 1 : equalSidesCount;
  }, 0);

  const triangleType = EQUAL_SIDE_COUNT_TO_TYPE_MAP[equalSideTotal];
  if (!triangleType) {
    throw new Error("Unexpected server error calculating triangle type");
  }
  return triangleType;
};

module.exports = { getTriangleType, validateTriangleSides };
