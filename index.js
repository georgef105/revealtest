const express = require("express");
const { getTriangleType, validateTriangleSides } = require("./utils");

const app = express();
const PORT = 3000;

app.get("/triangle", (req, res) => {
  const { a, b, c } = req.query;

  try {
    const validationState = validateTriangleSides(a, b, c);
    if (!validationState.isValid) {
      res.status(400).send(validationState.message || "bad request");
      return;
    }

    res.send(getTriangleType(a, b, c));
  } catch (error) {
    res.status(500).send(error.message);
  }
});

app.listen(PORT, () => {
  console.log(`Server running at port: ${PORT}`);
});
